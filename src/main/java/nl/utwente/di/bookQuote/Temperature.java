package nl.utwente.di.bookQuote;

import java.io.*;

import jakarta.servlet.*;
import jakarta.servlet.http.*;

/**
 * Example of a Servlet that gets an ISBN number and returns the book price
 */

public class Temperature extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Converter converter;

    public void init() throws ServletException {
        converter = new Converter();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Temperature";

        String celString = request.getParameter("celsius");
        double tempCelsius = 0.0;

        if (celString != null && !celString.trim().isEmpty()) {
            try {
                tempCelsius = Double.parseDouble(celString);
            } catch (NumberFormatException e) {
                out.println("Invalid temperature");
                return;
            }
        } else {
            out.println("Temperature is empty");
            return;
        }

        // Done with string concatenation only for the demo
        // Not expected to be done like this in the project
        out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celsius: " +
                celString + "\n" +
                "  <P>Temperature in Fahrenheit: " +
                converter.getFarenheit(tempCelsius) +
                "</BODY></HTML>");
    }

}

